package scalaStanNLP

import org.scalatest.FunSuite
import edu.stanford.nlp._

class NLPTest extends FunSuite {

  val s = Sentence("Prasanna has a Nikon E35 SLR and he takes nice pics out of it on my many occasions")

  test("test if the code could do word taggin") {
    println("words length " + s.words.mkString("  ::  "))
    val fm = s.parse.children()
    println("names " + s.ner.mkString(" :: "))
    println(s.namedEntities)
  }
}